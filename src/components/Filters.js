import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Paper, Checkbox, FormControl, FormLabel, FormGroup, FormControlLabel } from '@material-ui/core';

import {setFilters} from '../actions'
import '../App.css';

class Filters extends Component {

  handleChange = name => {
    this.props.setFilters(name);
  };

  render() {
    const { filters } = this.props;

    return (
      <Paper className='Filters'>
        <div className="FiltersHeader">Filters</div>
        <FormControl component="fieldset" >
          <FormLabel component="legend">Publisher</FormLabel>
          <FormGroup>
            {
              Object.keys(filters).map(key => (
                <FormControlLabel
                  key={key}
                  label={filters[key].label}
                  control={<Checkbox checked={filters[key].checked}
                                     onChange={() => this.handleChange(key)} value={key} />}
                />
              ))
            }

          </FormGroup>
        </FormControl>
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  error: state.error,
  heroes: state.heroes,
  filters: state.filters
});

const mapDispatchToProps = {setFilters};

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
