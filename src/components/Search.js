import React, {Component} from 'react';
import {connect} from 'react-redux';
import { TextField, Button } from '@material-ui/core';

import { setSearch } from '../actions';
import '../App.css';

class Search extends Component {

  state = {
    search: '',
  };

  handleSearch = event => {
    this.setState({
      ...this.state,
      search: event.target.value,
    })
  };

  onSearch = () => {
    this.props.setSearch(this.state.search)
  };

  render() {
    const { search } = this.state;

    return (
      <div className='Search'>
        <TextField
          id='outlined-name'
          label='By name'
          className='SearchInput'
          value={search}
          onChange={this.handleSearch}
          margin='normal'
          variant='outlined'
        />
        <Button variant="outlined"
                className={'SearchBtn'}
                onClick={this.onSearch}
        >
          Search
        </Button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  error: state.error,
  heroes: state.heroes
});

const mapDispatchToProps = { setSearch };

export default connect(mapStateToProps, mapDispatchToProps)(Search);
