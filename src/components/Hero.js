import React, {Component} from 'react';
import Paper from '@material-ui/core/Paper';
import '../App.css';
import { setHero } from '../actions'
import {connect} from "react-redux";

class Hero extends Component {
  state = {
    hero: this.props.hero || {}
  };

  goTo = route => {
    this.props.setHero(this.state.hero);
    this.props.history.push(route);
  };

  render() {
    const { hero } = this.state;

    return (
      <Paper className='Hero' onClick={() => this.goTo(`/hero/${hero.id}`)}>
        <img src={hero.images.sm} alt={hero.name} className='HeroImage' />
        <div className="HeroDescription">
          <div className="HeroItem">
            <span>
              Name:
            </span>
            { hero.name }
          </div>
          <div className="HeroItem">
            <span>
              FullName:
            </span>
            { hero.biography.fullName }
          </div>
          <div className="HeroItem">
            <span>
              Race:
            </span>
            { hero.appearance.race }
          </div>
          <div className="HeroItem">
            <span>
              Gender:
            </span>
            { hero.appearance.gender }
          </div>
          <div className="HeroItem">
            <span>
              Publisher:
            </span>
            { hero.biography.publisher }
          </div>
        </div>
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  error: state.error,
});

const mapDispatchToProps = { setHero };

export default connect(mapStateToProps, mapDispatchToProps)(Hero);
