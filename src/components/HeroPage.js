import React, { Component } from 'react';
import { Paper, Breadcrumbs } from '@material-ui/core';
import '../App.css';
import { getHero } from '../actions';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class HeroPage extends Component {

  componentDidMount() {
    const { hero } = this.props;
    if (!Object.keys(hero).length) {
      const id = this.props.history.location.pathname.split('/');
      this.props.getHero(id[2])
    }
  }

  showProperty = (item, key) => {
    return (
      <div className="HeroItem" key={key}>
        { isNaN(key)
            ? (
              <span>
                { key.charAt(0).toUpperCase() + key.slice(1) }:
              </span>
            )
            : null
        }

        { typeof item != 'object'
          ? item
          : Object.keys(item).map(key => (
            this.showProperty(item[key], key)
          ))
        }
      </div>
    )
  };

  render() {
    const { hero } = this.props;

    return (
      <div className='HeroPage'>
        <Breadcrumbs aria-label='breadcrumb'>
          <Link to={'/'} color='inherit'>
            Home
          </Link>
        </Breadcrumbs>
        {
          Object.keys(hero).length ?
            <Paper className='SingleHero'>
              <img src={hero.images.md} alt={hero.name} className='SingleHeroImage' />
              <div className="HeroDescription">
                {
                  Object.keys(hero).map(key => (
                    this.showProperty(hero[key], key)
                  ))
                }
              </div>
            </Paper> : <div>Loading...</div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  error: state.error,
  hero: state.hero,
});

const mapDispatchToProps = { getHero };

export default connect(mapStateToProps, mapDispatchToProps)(HeroPage);
