import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Hero from './Hero';
import Filters from './Filters';
import Search from './Search';

import {getHeroesRequest} from '../actions';
import '../App.css';

class Home extends Component {
  componentDidMount() {
    this.props.getHeroesRequest();
  }

  multiPropsFilter = (heroes, filters) => {
    if (!filters.length) {
      return heroes
    }

    return heroes.reduce((currentFilters, item) => {
      if (!item || !item.biography || !item.biography.publisher) {
        return currentFilters
      }

      filters.forEach(filter => {
        if (filter.includes(item.biography.publisher)) {
          currentFilters.push(item)
        }
      });

      return currentFilters
    }, []);
  };

  render() {
    let   { heroes } = this.props;
    const { filters, history } = this.props;

    const currentFilters =  Object.keys(filters).reduce((currentFilters, item) => {
      if (filters[item].checked) {
        currentFilters.push(filters[item].label);
      }
      return currentFilters;
    }, []);

    heroes = this.multiPropsFilter(heroes, currentFilters)

    return (
      <Fragment>
        <Filters/>
        <div className='HeroesList'>
          <Search />
          {
            heroes.map(item => (
              <Hero key={item.id} hero={item} history={history}/>
            ))
          }
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  error: state.error,
  heroes: state.heroes.filter((hero) => {
    return hero.name.toLowerCase().includes(state.search.toLowerCase())
  }),
  filters: state.filters,
});

const mapDispatchToProps = {getHeroesRequest};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
