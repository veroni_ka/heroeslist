import React, {Component} from 'react';
import {connect} from 'react-redux';
import Home from './components/Home';
import HeroPage from './components/HeroPage';
import { Route, Switch } from 'react-router-dom';
import './App.css';

class App extends Component {

  render() {

    return (
      <div className='App'>
        <Switch>
          <Route path='/'
                 render={({history})  => (
                   <Home
                     history={history}
                   />)}
                 exact
          />
          <Route path='/hero/:id'
                 render={({history})  => (
                   <HeroPage
                     history={history}
                 />)}
          />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  error: state.error,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(App);
