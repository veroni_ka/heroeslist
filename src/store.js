import {
  createStore,
  applyMiddleware,
} from 'redux'
import rootReducer from './reducers'
import axios from 'axios'
import axiosMiddleware from 'redux-axios-middleware';

const client = axios.create({
  baseURL:'https://akabab.github.io/superhero-api/api',
  responseType: 'json'
});

const createAppStore = (initState = {}) => {
  const store = createStore(
    rootReducer,
    applyMiddleware(
      axiosMiddleware(client),
    )
  );

  return store
};

export default createAppStore;