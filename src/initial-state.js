export default {
  isLoading: false,
  heroes: [],
  error: null,
  search: '',
  hero: {},
  filters: {
    marvel: {
      label: 'Marvel Comics',
      checked: false,
    },
    darkHorse: {
      label: 'Dark Horse Comics',
      checked: false,
    },
    dc: {
      label: 'DC Comics',
      checked: false,
    },
    nbc: {
      label: 'NBC - Heroes',
      checked: false,
    },
  },
}
