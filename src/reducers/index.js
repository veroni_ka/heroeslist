import {
  GET_HEROES_REQUEST,
  GET_HEROES_REQUEST_SUCCESS,
  GET_HEROES_REQUEST_FAILURE,
  SEARCH_HEROES,
  SET_FILTERS,
  SET_HERO,
  GET_HERO,
  GET_HERO_SUCCESS,
  GET_HERO_FAILURE,
} from '../actions/index'
import initState from '../initial-state'

const reducer = (state = initState, action) => {
  switch (action.type) {
    case GET_HEROES_REQUEST:
      return {...state, isLoading: true};
    case GET_HEROES_REQUEST_SUCCESS:
      return {
        ...state,
        heroes: action.payload.data,
        isLoading: false,
      };
    case GET_HEROES_REQUEST_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
      };
    case SEARCH_HEROES:
      return {
        ...state,
        search: action.payload,
      };
    case SET_FILTERS:
      return {
        ...state,
        filters: {
          ...state.filters,
          [action.payload]: {
            checked: !state.filters[action.payload].checked,
            label: state.filters[action.payload].label,
          }
        },
      };
    case SET_HERO:
      return {
        ...state,
        hero: action.payload
      };
    case GET_HERO:
      return {...state, isLoading: true};
    case GET_HERO_SUCCESS:
      return {
        ...state,
        hero: action.payload.data,
        isLoading: false,
      };
    case GET_HERO_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default reducer;