export const GET_HEROES_REQUEST = 'GET_HEROES_REQUEST';
export const GET_HEROES_REQUEST_SUCCESS = 'GET_HEROES_REQUEST_SUCCESS';
export const GET_HEROES_REQUEST_FAILURE = 'GET_HEROES_REQUEST_FAILURE';

export const GET_HERO = 'GET_HERO';
export const GET_HERO_SUCCESS = 'GET_HERO_SUCCESS';
export const GET_HERO_FAILURE = 'GET_HERO_FAILURE';

export const SEARCH_HEROES = 'SEARCH_HEROES';
export const SET_FILTERS = 'SET_FILTERS';
export const SET_HERO = 'SET_HERO';

export const getHeroesRequest = () => ({
  type: GET_HEROES_REQUEST,
  payload: {
    request: {
      url: '/all.json',
    }
  }
});

export const getHero = payload => ({
  type: GET_HERO,
  payload: {
    request: {
      url: `/id/${payload}.json`,
    }
  }
});

export const setSearch = payload => ({
  type: SEARCH_HEROES,
  payload
});

export const setFilters = payload => ({
  type: SET_FILTERS,
  payload
});

export const setHero = payload => ({
  type: SET_HERO,
  payload
});